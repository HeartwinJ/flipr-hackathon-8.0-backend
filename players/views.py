from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import JSONParser
from django.http.response import JsonResponse

from .models import Player
from matches.models import Match
from .serializers import PlayerSerializer

def getPlayers(data):
  players = set()
  team1 = data['info']['teams'][0]
  team2 = data['info']['teams'][1]
  for inn in data['innings']:
    for dely in inn[list(inn.keys())[0]]['deliveries']:
      if inn[list(inn.keys())[0]]['team'] == team1:
        curTeam = team1
        oppTeam = team2
      else:
        oppTeam = team1
        curTeam = team2
      players.add((curTeam, dely[list(dely.keys())[0]]['batsman']))
      players.add((curTeam, dely[list(dely.keys())[0]]['non_striker']))
      players.add((oppTeam, dely[list(dely.keys())[0]]['bowler']))
  return players

@csrf_exempt
def playersApi(request):
  if request.method == 'GET':
    players = Player.objects.all()
    players_serializer = PlayerSerializer(players, many = True)
    return JsonResponse(players_serializer.data, safe=False)

  elif request.method == 'POST':
    players_data = JSONParser().parse(request)
    players_serializer = PlayerSerializer(data=players_data)
    if players_serializer.is_valid():
      players_serializer.save()
      return JsonResponse('Added Successfully!', safe=False)
    return JsonResponse('Failed to Add!', safe=False)

  # elif request.method == 'PUT':
  #   players_data = JSONParser().parse(request)
  #   player = Player.objects.get(id = players_data['id'])
  #   players_serializer = PlayerSerializer(player, data=players_data)
  #   if players_serializer.is_valid():
  #     players_serializer.save()
  #     return JsonResponse('Updated Successfully!', safe=False)
  #   return JsonResponse('Failed to Update!', safe=False)
    
  # elif request.method == 'DELETE':
  #   player = Player.objects.get(id = id)
  #   player.delete()
  #   return JsonResponse('Deleted Sucessfully!', safe=False)
  # return JsonResponse('Failed to Delete!', safe=False)

@csrf_exempt
def matchPlayersApi(request, matchId):
  if request.method == 'GET':
    match = Match.objects.get(id = matchId)
    playerRes = []
    for player in getPlayers(match.matchJson):
      playerData = Player.objects.get(name=player[1])
      playerRes.append(playerData)
    players_serializer = PlayerSerializer(playerRes, many = True)
    print(players_serializer)
    return JsonResponse(players_serializer.data, safe=False)
