from django.conf.urls import url
from . import views

urlpatterns = [
  url(r'^$', views.playersApi),
  url(r'^([0-9]+)$', views.matchPlayersApi)
]
