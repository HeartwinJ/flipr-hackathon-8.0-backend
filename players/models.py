from django.db import models

class Player(models.Model):
  name = models.CharField(max_length=50)
  credit = models.DecimalField(max_digits=4, decimal_places=2)