from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import JSONParser
from django.http.response import JsonResponse

from .models import Match
from .serializers import MatchSerializer

@csrf_exempt
def matchesListApi(request):
  if request.method == 'GET':
    matches = Match.objects.all()
    matches_serializer = MatchSerializer(matches, many = True)
    return JsonResponse(matches_serializer.data, safe=False)

@csrf_exempt
def matchApi(request, id):
  if request.method == 'GET':
    match = Match.objects.get(id = id)
    matches_serializer = MatchSerializer(match, many = False)
    return JsonResponse(matches_serializer.data, safe=False)