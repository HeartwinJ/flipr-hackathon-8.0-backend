from django.conf.urls import url
from . import views

urlpatterns = [
  url(r'^$', views.matchesListApi),
  url(r'^([0-9]+)$', views.matchApi)
]
